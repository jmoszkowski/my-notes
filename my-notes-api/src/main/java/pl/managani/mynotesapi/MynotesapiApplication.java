package pl.managani.mynotesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MynotesapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MynotesapiApplication.class, args);
    }

}
