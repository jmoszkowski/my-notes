package pl.managani.mynotesapi.repositories;

import pl.managani.mynotesapi.model.Record;
import org.springframework.data.repository.CrudRepository;


public interface RecordsRepository extends CrudRepository<Record, Integer> {
}
