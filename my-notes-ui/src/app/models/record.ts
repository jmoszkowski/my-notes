export class Record {
  id: number;
  date: Date;
  title: string;
  content: string;
}
