import { Component, OnInit } from '@angular/core';
import { Record } from '../models/record';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent implements OnInit {

  records: Record[];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('http://localhost:8080/records').subscribe((data: Record[]) => this.records = data);
  }
}
