create table record_topics
(
    record_id int not null,
    topic_id  int not null
);

create table records
(
    id      int auto_increment
        primary key,
    date    date not null,
    title   text not null,
    content text null
);

create table topics
(
    id         int auto_increment,
    topic_name text not null,
    constraint topics_id_uindex
        unique (id)
);

alter table topics
    add primary key (id);

create table user_records
(
    user_id   int not null,
    record_id int not null
);

create table user_topics
(
    user_id  int not null,
    topic_id int not null
);

create table users
(
    id    int auto_increment
        primary key,
    email varchar(100) not null,
    constraint users_email_uindex
        unique (email)
);


