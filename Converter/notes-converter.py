#!/usr/bin/env python3
import argparse
import json


def read_args():
    parser = argparse.ArgumentParser(description='Convert notes from txt to json')
    parser.add_argument('-i', '--input', help='input txt file', required=True)
    parser.add_argument('-o', '--output', help='output json file', required=True)
    args = parser.parse_args()
    return args.input, args.output


def read_file(input_file):
    with open(input_file, 'r') as f:
        return [line.strip() for line in f.readlines()]


def is_separator(line):
    if '*' * 5 in line:
        return True


def read_content(line, records):
    if is_separator(line):
        records.append({})
        return read_headers
    record = records.pop()
    if not 'content' in record.keys():
        record['content'] = []
    record['content'].append(line)

    records.append(record)
    return read_content


def read_headers(line, records):
    record = records.pop()
    if all(elem in record.keys() for elem in ['topics', 'date', 'title']):
        records.append(record)
        return read_content
    if line.lower().startswith('data:'):
        record['date'] = line.split(':')[1].strip()
    else:
        if 'topics' not in record.keys():
            record['topics'] = [topic.strip() for topic in line.split(':')[1].strip().split(',')]
        else:
            record['title'] = line.split(':')[1].strip()
    records.append(record)
    return read_headers


def goto_first_record(line, records):
    if is_separator(line):
        records.append({})
        return read_headers
    return goto_first_record


def main():
    (input_file, output_file) = read_args()
    records = []
    reader = goto_first_record
    for line in read_file(input_file):
        reader = reader(line, records)

    with open(output_file, 'w') as f:
        json.dump(records, f, indent=2, ensure_ascii=False)


if __name__ == '__main__':
    main()
