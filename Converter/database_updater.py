#!/usr/bin/env python3
import argparse
import json

import MySQLdb


def read_args():
    parser = argparse.ArgumentParser(description='Upload notes from json file to database')
    parser.add_argument('-i', '--input', help='input json file', required=True)
    parser.add_argument('-a', '--host', help='database host', required=True)
    parser.add_argument('-u', '--user', help='database user', required=True)
    parser.add_argument('-p', '--password', help='database password', required=True)
    parser.add_argument('-d', '--database', help='databse name', required=True)
    parser.add_argument('-e', '--email', help='user email', required=True)
    args = parser.parse_args()
    return args


def make_user(email, cursor):
    if cursor.execute("select id from users where email = '{}'".format(email)):
        return cursor.fetchone()[0]
    cursor.execute("insert into users(email) values ('{}')".format(email))
    return make_user(email, cursor)


def make_topic(topic, record_id, user_id, cursor):
    # import ipdb; ipdb.set_trace()
    topic_id = None
    if cursor.execute("select id from topics where topic_name = '{}'".format(topic)):
        topic_id = cursor.fetchone()[0]
    else:
        cursor.execute("insert into topics(topic_name) values('{}')".format(topic))
        topic_id = cursor.lastrowid
    if not cursor.execute("select * from user_topics where user_id={} and topic_id={}".format(user_id, topic_id)):
        cursor.execute("insert into user_topics(user_id, topic_id) values({}, {})".format(user_id, topic_id))
    cursor.execute("insert into record_topics(record_id, topic_id) values({}, {})".format(record_id, topic_id))


def add_records(records, user_id, cursor):
    for record in records:
        statement = "insert into records(date, title, content) VALUES ('{}', '{}', '{}')".format(
            record['date'],
            record['title'].replace("'", "''"),
            "<p>{}</p>".format('<br />'.join(record['content'])).replace("'", "''")
        )
        print(statement)
        cursor.execute(statement)
        record_id = cursor.lastrowid
        for topic in record['topics']:
            make_topic(topic, record_id, user_id, cursor)


def main():
    args = read_args()
    conn = MySQLdb.connect(args.host, args.user, args.password)
    cursor = conn.cursor()
    cursor.execute('use {}'.format(args.database))

    with open(args.input, 'r') as f:
        records = json.load(f)
        user_id = make_user(args.email, cursor)
        add_records(records, user_id, cursor)


    conn.commit()
    cursor.close()
    conn.close()


if __name__ == '__main__':
    main()
